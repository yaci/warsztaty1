FROM ubuntu:16.04
MAINTAINER Yaci "jacek.maciejewski@cognifide.com"
RUN apt-get update && apt-get install -y nginx
RUN echo 'Wujek Vernon, wujek Vernon.' > /var/www/html/index.html
EXPOSE 4567